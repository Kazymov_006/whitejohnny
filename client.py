import os
import pickle
import socket
import threading
import sys
from typing import Union

import pygame

import settings as sts
from services import send_msg, Room, User

pygame.font.init()
pygame.init()
win = pygame.display.set_mode((sts.WIDTH, sts.HEIGHT))
pygame.display.set_caption("Client")

SERVER = os.environ.get("SERVER_HOST", "192.168.0.100")
ADDR = (SERVER, sts.PORT)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


class Button:
    def __init__(self, text, x, y, color):
        self.text = text
        self.x = x
        self.y = y
        self.color = color
        self.width = 250
        self.height = 100

    def draw(self, win):
        pygame.draw.rect(
            win, self.color, (self.x, self.y, self.width, self.height)
        )
        font = pygame.font.SysFont("comicsans", 40)
        text = font.render(self.text, 1, (255, 255, 255))
        win.blit(
            text,
            (
                self.x + round(self.width / 2) - round(text.get_width() / 2),
                self.y + round(self.height / 2) - round(text.get_height() / 2),
            ),
        )

    def click(self, pos):
        x1 = pos[0]
        y1 = pos[1]
        if (
            self.x <= x1 <= self.x + self.width
            and self.y <= y1 <= self.y + self.height
        ):
            return True
        else:
            return False


def redraw_window(win, user: User, room: Room):
    win.fill(sts.BG_COLOR)

    global current_room
    global current_user
    if current_user is None or current_room is None:
        font = pygame.font.SysFont("comicsans", 80)
        text = font.render("Waiting for second player", 1, (255, 0, 0), True)
        win.blit(
            text,
            (
                sts.WIDTH / 2 - text.get_width() / 2,
                sts.HEIGHT / 2 - text.get_height() / 2,
            ),
        )
        pygame.display.update()
    else:
        if room.state == sts.BETTING:
            font = pygame.font.SysFont("comicsans", 24)
            text = font.render(
                "How much do you want to bet?",
                True,
                sts.TXT_COLOR,
                sts.BG_COLOR,
            )
            win.blit(text, (1000, 400))
            text = font.render(
                f"room's current bank = {room.current_bet}",
                True,
                sts.TXT_COLOR,
                sts.BG_COLOR,
            )
            win.blit(text, (1000, 200))
            if user.prev_stage == sts.IDLE:
                for btn in btns[:3]:
                    btn.draw(win)
            else:
                text = font.render(
                    f"Wait until your opponents bet...",
                    True,
                    sts.TXT_COLOR,
                    sts.BG_COLOR,
                )
                win.blit(text, (1000, 500))
        elif room.state == sts.DRAWING:
            font = pygame.font.SysFont("comicsans", 24)
            text = font.render(
                f"Want some more? Current bet: {room.current_bet}. Your balance: {user.balance}.",
                True,
                sts.TXT_COLOR,
                sts.BG_COLOR,
            )
            ops = room.users[:]
            ops.remove(user)
            op = ops[0]
            my_cards_num = len(user.deck.cards)
            my_deck_coord = (
                (sts.WIDTH - ((sts.CARD_WIDH + 2) * my_cards_num)) // 2,
                sts.HEIGHT - 5 - sts.CARD_HEIGHT,
            )
            op_cards_num = len(op.deck.cards)
            op_deck_coord = (
                (sts.WIDTH - ((sts.CARD_WIDH + 2) * op_cards_num)) // 2,
                5,
            )
            print(
                f"у противника {op_cards_num} карт: "
                f"{list(map(str, op.deck.cards))}, "
                f"у Вас - {my_cards_num}: {list(map(str, user.deck.cards))}"
            )
            for i in range(my_cards_num):
                cord = (
                    my_deck_coord[0] + i * (sts.CARD_WIDH + 2),
                    my_deck_coord[1],
                )
                image = pygame.image.load(user.deck.cards[i].img)
                win.blit(
                    pygame.transform.scale(
                        image, (sts.CARD_WIDH, sts.CARD_HEIGHT)
                    ),
                    cord,
                )

            for i in range(op_cards_num):
                cord = (
                    op_deck_coord[0] + i * (sts.CARD_WIDH + 2),
                    op_deck_coord[1],
                )
                image = pygame.image.load(op.deck.cards[i].img)
                win.blit(
                    pygame.transform.scale(
                        image, (sts.CARD_WIDH, sts.CARD_HEIGHT)
                    ),
                    cord,
                )

            win.blit(text, (900, 488))
            for btn in btns[3:5]:
                btn.draw(win)
        elif room.state == sts.RESULTS:
            font = pygame.font.SysFont("comicsans", 24)
            if user.addr in list(map(lambda x: x.addr, room.winners)):
                text = font.render(
                    f"You won (heheHaHa)! Your balance: {user.balance}",
                    True,
                    sts.TXT_COLOR,
                    sts.BG_COLOR,
                )
            else:
                text = font.render(
                    f"You lost... (Grrrr!!!) Your balance: {user.balance}",
                    True,
                    sts.TXT_COLOR,
                    sts.BG_COLOR,
                )

            # умное логирование + вывод карт
            print("Results")
            ops = room.users[:]
            ops.remove(user)
            op = ops[0]
            my_cards_num = len(user.deck.cards)
            my_deck_coord = (
                (sts.WIDTH - ((sts.CARD_WIDH + 2) * my_cards_num)) // 2,
                sts.HEIGHT - 5 - sts.CARD_HEIGHT,
            )
            op_cards_num = len(op.deck.cards)
            op_deck_coord = (
                (sts.WIDTH - ((sts.CARD_WIDH + 2) * op_cards_num)) // 2,
                5,
            )
            my_cards_num = len(user.deck.cards)
            op_cards_num = len(op.deck.cards)
            print(
                f"у противника {op_cards_num} карт: "
                f"{list(map(str, op.deck.cards))}, "
                f"у Вас - {my_cards_num}: {list(map(str, user.deck.cards))}"
            )
            for i in range(my_cards_num):
                cord = (
                    my_deck_coord[0] + i * (sts.CARD_WIDH + 2),
                    my_deck_coord[1],
                )
                image = pygame.image.load(user.deck.cards[i].img)
                win.blit(
                    pygame.transform.scale(
                        image, (sts.CARD_WIDH, sts.CARD_HEIGHT)
                    ),
                    cord,
                )

            for i in range(op_cards_num):
                cord = (
                    op_deck_coord[0] + i * (sts.CARD_WIDH + 2),
                    op_deck_coord[1],
                )
                image = pygame.image.load(op.deck.cards[i].img)
                win.blit(
                    pygame.transform.scale(
                        image, (sts.CARD_WIDH, sts.CARD_HEIGHT)
                    ),
                    cord,
                )

            win.blit(text, (900, 200))
            btns[5].draw(win)
    pygame.display.update()


def send(msg):
    send_msg(msg, client, sts.HEADER, sts.FORMAT)


btns = [
    Button("BET 100", 50, 100, sts.BTN_COLOR),
    Button("BET 300", 50, 210, sts.BTN_COLOR),
    Button("BET 500", 50, 320, sts.BTN_COLOR),
    Button("DRAW", 310, 550, sts.BTN_COLOR),
    Button("STOP_DRAW", 570, 550, sts.BTN_COLOR),
    Button("RESUME", 1400, 700, sts.BTN_COLOR),
]


def listen_to_server():  # and play the game
    wait_screen()
    while True:
        msg_len = int(client.recv(sts.HEADER).decode(sts.FORMAT))
        if msg_len:
            decoded_msg = pickle.loads(client.recv(msg_len))
            # если все игроки сходили, то перерисовываем окощько
            if decoded_msg["all_went"]:
                global current_user
                global current_room
                current_user = decoded_msg["user"]
                current_room = decoded_msg["room"]
                if current_user.drawing:
                    for user in current_user.room.users:
                        if user.deck.sum_cost() > 21:
                            send(sts.STOP_DRAW_MESSAGE)
                # redraw_window(win, decoded_msg["room"], decoded_msg["user"])


def main_game_loop():
    run = True
    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                for btn in btns:
                    if btn.click(pos):
                        #  отправить сообщение серверу
                        send(btn.text)

        redraw_window(win, current_user, current_room)


current_user: Union["User", None] = None
current_room: Union["Room", None] = None


def menu_screen():
    in_menu = True
    while in_menu:
        win.fill((128, 128, 128))
        font = pygame.font.SysFont("comicsans", 60)
        text = font.render("Click to play", 1, (255, 0, 0))
        win.blit(text, (100, 200))
        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                in_menu = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                in_menu = False

    client.connect(ADDR)
    listen_thread = threading.Thread(target=listen_to_server, daemon=True)
    listen_thread.start()
    main_game_loop()


def wait_screen():
    font = pygame.font.SysFont("comicsans", 80)
    text = font.render("Waiting for Player...", 1, (255, 0, 0), True)
    win.blit(
        text,
        (
            sts.WIDTH / 2 - text.get_width() / 2,
            sts.HEIGHT / 2 - text.get_height() / 2,
        ),
    )
    pygame.display.update()


if __name__ == "__main__":
    try:
        print('to disconnect print "!DISCONNECT"')
        menu_screen()
    except KeyboardInterrupt:
        raise KeyboardInterrupt
    finally:
        send(sts.DISCONNECT_MESSAGE)
        sys.exit()
