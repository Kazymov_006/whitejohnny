import pickle
import socket
import sys
import threading

import settings as sts
from services import User, send_json, send_object

SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, sts.PORT)
DISCONNECT_MESSAGE = "!DISCONNECT"

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)


def handle_client(conn, addr):
    try:
        print(f"[NEW CONNECTIONS] {addr} connected.")
        user = User(conn, addr)
        while user.in_queue():
            pass
        user.room.handle_user_turn()  # отправка сообщения серверу, что мы приконнектились
        user.prev_stage = 0
        connected = True
        while connected:
            msg_length_b = conn.recv(sts.HEADER)
            if msg_length_b:
                msg_length = int(msg_length_b.decode(sts.FORMAT))
                msg_b = conn.recv(msg_length)
                msg = msg_b.decode(sts.FORMAT)
                print(f"[САПЩЕНЕ С ЮЗЕРА]{msg}")

                if (
                    user.room.state == sts.BETTING
                    and sts.BET_MESSAGE in msg
                    and user.prev_stage == sts.IDLE
                ):
                    bet = int(msg[4:])  # сообщение выглядит как "MSG [0-9]+"
                    if bet <= user.balance:
                        user.room.current_bet += bet
                        user.balance -= bet
                        user.room.handle_user_turn()
                        user.prev_stage = sts.BETTING
                elif user.room.state == sts.DRAWING and user.prev_stage in [
                    sts.DRAWING,
                    sts.BETTING,
                ]:
                    if sts.DRAW_MESSAGE == msg[:4] and user.drawing:
                        user.room.handle_user_turn()
                        user.prev_stage = sts.DRAWING
                    elif sts.STOP_DRAW_MESSAGE == msg[:9] and user.drawing:
                        user.drawing = False
                        user.prev_stage = sts.DRAWING
                        user.room.handle_user_turn()
                elif (
                    sts.RESUME_COMMAND in msg and user.room.state == sts.RESULTS
                ):
                    user.room.handle_user_turn()
                elif msg == DISCONNECT_MESSAGE:
                    connected = False
                    user.leave()
                user.room.send_info_to_all_users()
    except Exception as e:
        print(f"[SERVER] {e}")
        raise e
    finally:
        conn.close()


def start():
    server.listen()
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(
            target=handle_client, args=(conn, addr), daemon=True
        )
        thread.start()

        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")


if __name__ == "__main__":
    print("[STARTING] The server is starting...")
    print(ADDR)
    start()
