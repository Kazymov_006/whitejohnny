from pathlib import Path


HEADER = 64
PORT = 5050
FORMAT = "utf-8"
DISCONNECT_MESSAGE = "!DISCONNECT"

# possible states of the game
IDLE = 0
BETTING = 1
DRAWING = 2
FINISHING = 3
RESULTS = 4

MAX_ROOM_MEMBERS = 2
MIN_ROOM_MEMBERS = 2

INITIAL_MONEY = 1000
BET_MESSAGE = "BET"  # собщение для сервера будет выглядеть как "BET 500"
DRAW_MESSAGE = "DRAW"
STOP_DRAW_MESSAGE = "STOP_DRAW"
RESUME_COMMAND = "RESUME"

BASE_DIR = Path(__file__).resolve().parent
MEDIA_DIR = BASE_DIR / "media"

# PyGame
WIDTH = 1920
HEIGHT = 1080
BG_COLOR = (13, 90, 18)
TXT_COLOR = (255, 255, 255)
BTN_COLOR = (5, 160, 15)
CARD_WIDH = 223
CARD_HEIGHT = 324


if __name__ == "__main__":
    print(BASE_DIR)
