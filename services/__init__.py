import json
import pickle
import random
import threading
from typing import List, Tuple

from settings import (
    MAX_ROOM_MEMBERS,
    MIN_ROOM_MEMBERS,
    IDLE,
    BETTING,
    DRAWING,
    FINISHING,
    INITIAL_MONEY,
    HEADER,
    FORMAT,
    MEDIA_DIR,
    RESULTS,
)


lock = threading.Lock()


class User:
    users_queue: List["User"] = []
    num_users = 0
    max_users = 100

    def __init__(self, socket, addr):
        self.socket = socket
        self.addr = addr
        self.room = None
        self.balance = 0
        self.prev_stage = 0
        self.deck = CardsCollection(mode="empty")
        self.drawing = True  # поле для стадии DRAWING, False, если игрок отказывается брать карты
        self.went = False  # поля для определния того, кто сходил, а кто нет
        User.num_users += 1
        self.release()

    def leave(self):
        if self.room:
            self.room.remove(self)
        User.num_users -= 1

    def release(self):
        if not Room.free_rooms and User.num_users <= User.max_users:
            Room.free_rooms.append(Room())
        if User.num_users <= User.max_users:
            self.room = Room.free_rooms[0]
            self.room.add(self)
        else:
            User.users_queue.append(self)

    def in_queue(self):
        return self in User.users_queue

    # переопределить это
    # def send_b(self, msg_len_b, msg_b):
    #     self.interlocutor.socket.send(msg_len_b)
    #     self.interlocutor.socket.send(msg_b)
    #
    # def send(self, msg, header, formt):
    #     send_msg(msg, self.interlocutor.socket, header, formt)


class Room:
    free_rooms: List["Room"] = []

    def __init__(
        self, max_members=MIN_ROOM_MEMBERS, min_members=MAX_ROOM_MEMBERS
    ):
        self.max_members = max_members
        self.min_members = min_members
        self.deck = CardsCollection(mode="filled")
        self.state = 0  # check settings for more info about states
        self.users: List["User"] = []
        self.removed_users: List["User"] = []
        self.current_bet = 0
        self.turns_on_current_state = 0
        self.winners: List["User"] = []
        Room.free_rooms.append(self)

    def send_info_to_all_users(self):
        _json = {"all_went": True, "room": RoomMock(self)}
        # Cross Join (Teta)
        for user in self.users:
            for user_mock in _json["room"].users:
                # меняем джейсон для каждого юзера
                if user.addr == user_mock.addr:
                    _json["user"] = user_mock
                    send_object(_json, user.socket, HEADER)
        _json["room"].kill()

    def handle_user_turn(self):
        self.turns_on_current_state += 1
        if (
            self.state == IDLE
            and self.turns_on_current_state == self.min_members
        ):
            self.deck.shuffle()
            for user in self.users:
                user.balance = INITIAL_MONEY
                user.deck.cards.append(self.deck.pop())
                user.deck.cards.append(self.deck.pop())
            self.turns_on_current_state = 0
            self.state = BETTING
            # отправить команду юзерам для отрисовки
            self.send_info_to_all_users()
        elif (
            self.state == DRAWING
            and self.turns_on_current_state == self.get_members_num()
        ):
            users_drawing = list(map(lambda user: user.drawing, self.users))
            if not any(
                users_drawing
            ):  # если ни один юзер не берет карты, идем к следующей стадии
                self.state = FINISHING
            else:
                for user in self.users:
                    if user.drawing:
                        user.deck.cards.append(self.deck.pop())
                self.turns_on_current_state = len(
                    list(filter(lambda user: not user.drawing, self.users))
                )  # ставим кол-во ходов равное кол-ву не берущих карты юзеров
                return
        if (
            self.state == IDLE
            and self.turns_on_current_state == self.min_members
            or self.state != IDLE
            and self.turns_on_current_state == self.get_members_num()
            or self.state == FINISHING
        ):
            self.turns_on_current_state = 0
            if self.state == FINISHING:
                self.winners = self.decide_winner()
                self.send_info_to_all_users()
            if self.state == RESULTS:
                self.refresh()
            self.state = (self.state + 1) % 5  # 5 - максимальное кол-во states
            self.send_info_to_all_users()

    def catch_users(self):
        for i in range(self.max_members - self.get_members_num()):
            if not User.users_queue or User.num_users >= User.max_users:
                break
            user = User.users_queue.pop(0)
            user.release()

    def is_free(self):
        return self in Room.free_rooms

    def remove(self, user: "User"):
        self.turns_on_current_state -= 1
        self.users.remove(user)
        self.removed_users.append(user)

    def add(self, user: "User"):
        self.users.append(user)

    def get_members_num(self):
        return len(self.users)

    def decide_winner(self) -> List["User"]:
        """
        :return: список победителей
        """
        # убираем игроков, которые точно проиграли, тут также нет и покинувших
        # игру людей
        pretendents = list(
            filter(lambda usr: usr.deck.sum_cost() <= 21, self.users)
        )
        if not pretendents:
            winners = self.users
        else:  # выбираем тех, кто набрал наивысший балл среди всех
            max_score = max(
                list(map(lambda usr: usr.deck.sum_cost(), pretendents))
            )
            winners = list(
                filter(
                    lambda usr: usr.deck.sum_cost() == max_score, pretendents
                )
            )
        # начисление выигрыша
        bank = self.current_bet
        for usr in winners:
            usr.balance += bank // len(winners)
            self.current_bet -= bank // len(winners)

        return winners

    def refresh(self):
        # удаляем из памяти отключившихся пользователей
        del self.removed_users
        self.removed_users = []

        del self.deck
        self.deck = CardsCollection(mode="filled")
        self.deck.shuffle()

        # обновляем колоду и руки
        for user in self.users:
            del user.deck
            user.deck = CardsCollection(mode="empty")
            user.deck.cards.append(self.deck.pop())
            user.deck.cards.append(self.deck.pop())
            user.drawing = True
            user.prev_stage = IDLE

        # обновляем состояния
        self.state = 0
        self.turns_on_current_state = 0


class CardBase:
    VALS = [str(i) for i in range(2, 11)]
    VALS.extend(["Jack", "Queen", "King", "Ace"])
    SUITS = ["Diamonds", "Hearts", "Clubs", "Spades"]
    COST = {str(i): i for i in range(2, 11)}
    for val in ["Jack", "Queen", "King"]:
        COST[val] = 10
    COST["Ace"] = 11

    def __init__(self, value, suit):
        self.value = value
        self.suit = suit
        self.cost = CardBase.COST[value]

    def __str__(self):
        return f"{self.value.lower()}_of_{self.suit.lower()}"


class Card(CardBase):
    def __init__(self, *args, **kwargs):
        super(Card, self).__init__(*args, **kwargs)
        self.img = (
            MEDIA_DIR / "PNG-cards-1.3" / f"{self.value.lower()}_of_"
            f"{self.suit.lower()}.png"
        )


class CardsCollection:
    def __init__(self, mode="empty"):
        self.cards: List["Card"] = []
        if mode == "filled":
            self.fill()

    def fill(self):
        for val in Card.VALS:
            for suit in Card.SUITS:
                self.cards.append(Card(val, suit))

    def shuffle(self):
        random.shuffle(self.cards)

    def sum_cost(self):
        aces_num = 0
        s = 0
        for card in self.cards:
            s += card.cost
            if card.value == "Ace":
                aces_num += 1
        for i in range(aces_num):
            if s > 21:
                s -= 10
            else:
                break
        return s

    def pop(self, _index=-1):
        return self.cards.pop(_index)


class UserMock(object):
    def __init__(self, orig: "User", mock_room):
        self.addr = orig.addr
        self.room = mock_room
        self.balance = orig.balance
        self.prev_stage = orig.prev_stage
        self.deck = orig.deck
        self.drawing = orig.drawing
        self.went = orig.drawing

    def kill(self):
        self.room.kill()
        UserMock.USERS = []

    def _kill(self):
        UserMock.USERS = []
        del self


class RoomMock:
    def __init__(self, orig: "Room"):
        self.max_members = orig.max_members
        self.min_members = orig.min_members
        self.deck = orig.deck
        self.state = orig.state
        self.users: List["UserMock"] = list(
            map(lambda usr: UserMock(usr, self), orig.users)
        )
        self.removed_users: List["UserMock"] = list(
            map(lambda usr: UserMock(usr, self), orig.removed_users)
        )
        self.current_bet = orig.current_bet
        self.turns_on_current_state = orig.turns_on_current_state
        self.winners: List["UserMock"] = list(
            map(lambda usr: UserMock(usr, self), orig.winners)
        )

    def kill(self):
        for i in range(len(self.users)):
            self.users[i]._kill()
        for i in range(len(self.removed_users)):
            self.removed_users[i]._kill()


def send_msg(msg, sockt, header, formt):
    message = msg.encode(formt)
    msg_length = len(message)
    send_length = str(msg_length).encode(formt)
    send_length += b" " * (header - len(send_length))
    lock.acquire()
    sockt.send(send_length)
    sockt.send(message)
    lock.release()


def send_json(_dict, sockt, header, formt):
    data = bytes(json.dumps(_dict), encoding=formt)
    msg_length = len(data)
    send_length = str(msg_length).encode(formt)
    send_length += b" " * (header - len(send_length))
    sockt.send(send_length)
    sockt.send(data)


def send_object(obj, sockt, header):
    to_send = pickle.dumps(obj)
    msg_length = len(to_send)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b" " * (header - len(send_length))
    try:
        lock.acquire()
        sockt.send(send_length)
        sockt.send(to_send)
        lock.release()
    except Exception as e:
        print(e)
